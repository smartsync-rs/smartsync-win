#![cfg_attr(not(feature = "debug"), windows_subsystem = "windows")]

use nwd::{NwgPartial, NwgUi};
use smartsync_core::{config, registry, Backup, DeviceConfig, FileSync};
use std::cell::RefCell;
use std::error::Error;
use stretch::{
    geometry::Size,
    style::{Dimension, FlexDirection, JustifyContent, Style},
};

use nwg::NativeUi;

#[derive(Default, NwgPartial)]
pub struct Config {
    #[nwg_control(size: (800, 600), position: (400, 200), title: "Configuration", flags: "WINDOW")]
    window: nwg::Window,

    #[nwg_layout(parent: window, flex_direction: FlexDirection::Column)]
    layout: nwg::FlexboxLayout,

    labels: RefCell<Vec<nwg::Label>>,

    initialized: RefCell<bool>,
}

impl Config {
    fn show(&self) {
        self.window.set_visible(true);
        let mut initialized = self.initialized.borrow_mut();
        if !*initialized {
            *initialized = true;
            self.init();
        }
    }

    fn init(&self) {
        match registry::load_registry() {
            Ok(reg) => {
                for b in reg.backups {
                    self.add_backup(&b);
                }
            }
            Err(error) => {
                nwg::modal_error_message(
                    &self.window,
                    "Error Loading Registry",
                    &error.to_string(),
                );
            }
        }
    }

    fn add_backup(&self, backup: &Backup) {
        match config::load_config(&backup.path) {
            Ok(config) => {
                for d in config.devices {
                    self.add_device(&d);
                }
            }
            Err(error) => {
                nwg::modal_error_message(
                    &self.window,
                    "Error Loading Configuration",
                    &error.to_string(),
                );
            }
        }
    }

    fn add_device(&self, device: &DeviceConfig) {
        // device name
        let name = format!("Device: {}", device.name);
        self.add_label(&name);
        for f in &device.files {
            self.add_sync(f);
        }
        if let Some(last_backup) = device.last_backup {
            let last_backup = format!("Last backup: {}", last_backup);
            self.add_label(&last_backup);
        }
    }

    fn add_sync(&self, file_sync: &FileSync) {
        let name = format!("Name: {}", file_sync.name);
        self.add_label(&name);

        let sources = "Sources:".to_owned();
        self.add_label(&sources);
        for s in file_sync.sources_str() {
            let source = format!("- {}", s);
            self.add_label(&source);
        }

        let dest = format!("Dest: {}", file_sync.dest_str());
        self.add_label(&dest);
    }

    fn add_label(&self, text: &str) {
        let mut label = Default::default();
        nwg::Label::builder()
            .text(text)
            .parent(&self.window)
            .build(&mut label)
            .unwrap();
        let style = Style {
            size: Size {
                width: Dimension::Auto,
                height: Dimension::Points(20.0),
            },
            justify_content: JustifyContent::FlexStart,
            ..Default::default()
        };
        self.layout.add_child(&mut label, style).unwrap();
        self.labels.borrow_mut().push(label);
    }
}

#[derive(Default, NwgUi)]
pub struct App {
    #[nwg_control(size: (1024, 768), position: (300, 100), title: "Smart Sync", flags: "WINDOW|VISIBLE")]
    #[nwg_events(OnWindowClose: [App::close])]
    window: nwg::Window,

    #[nwg_partial(parent: window)]
    config: Config,

    #[nwg_control(parent: window, text: "File")]
    file_menu: nwg::Menu,

    #[nwg_control(parent: file_menu, text: "Quit")]
    #[nwg_events(OnMenuItemSelected: [App::quit])]
    quit_menu_item: nwg::MenuItem,

    #[nwg_control(parent: window, text: "Config")]
    config_menu: nwg::Menu,

    #[nwg_control(parent: config_menu, text: "Configure Backups")]
    #[nwg_events(OnMenuItemSelected: [App::config])]
    config_menu_item: nwg::MenuItem,
}

impl App {
    fn config(&self) {
        self.config.show();
    }

    fn quit(&self) {
        self.window.close();
    }

    fn close(&self) {
        nwg::stop_thread_dispatch();
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    nwg::init()?;
    let _app = App::build_ui(Default::default())?;
    nwg::dispatch_thread_events();

    Ok(())
}
